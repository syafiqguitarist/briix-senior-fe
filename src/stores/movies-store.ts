import { defineStore } from 'pinia';
import { LocalStorage } from 'quasar';

interface MovieData {
  id: number;
  title: string;
  director: string;
  summary: string;
  genres: string[];
}

const initialData: MovieData[] = [
  {
    id: 1,
    title: 'Avatar (2009)',
    director: 'James Cameron',
    summary:
      'A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.',
    genres: ['Action', 'Adventure', 'Fantasy', 'Sci-Fi']
  },
  {
    id: 2,
    title: 'The Avengers (2012)',
    director: 'Joss Whedon',
    summary:
      "Earth's mightiest heroes must come together and learn to fight as a team if they are going to stop the mischievous Loki and his alien army from enslaving humanity.",
    genres: ['Action', 'Adventure', 'Sci-Fi']
  },
  {
    id: 3,
    title: 'Titanic (1997)',
    director: 'James Cameron',
    summary:
      'A seventeen-year-old aristocrat falls in love with a kind but poor artist aboard the luxurious, ill-fated R.M.S. Titanic.',
    genres: ['Drama', 'Romance']
  },
  {
    id: 4,
    title: 'Jurassic World (2015)',
    director: 'Colin Trevorrow',
    summary:
      'A new theme park, built on the original site of Jurassic Park, creates a genetically modified hybrid dinosaur, the Indominus Rex, which escapes containment and goes on a killing spree.',
    genres: ['Action', 'Adventure', 'Sci-Fi', 'Thriller']
  },
  {
    id: 5,
    title: 'The Lion King (1994)',
    director: 'Roger Allers, Rob Minkoff',
    summary:
      'Lion prince Simba and his father are targeted by his bitter uncle, who wants to ascend the throne himself.',
    genres: ['Animation', 'Adventure', 'Drama', 'Family', 'Musical']
  },
  {
    id: 6,
    title: 'The Dark Knight (2008)',
    director: 'Christopher Nolan',
    summary:
      'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.',
    genres: ['Action', 'Crime', 'Drama', 'Thriller']
  },
  {
    id: 7,
    title: 'Inception (2010)',
    director: 'Christopher Nolan',
    summary:
      'A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a C.E.O.',
    genres: ['Action', 'Adventure', 'Sci-Fi', 'Thriller']
  },
  {
    id: 8,
    title: 'Interstellar (2014)',
    director: 'Christopher Nolan',
    summary:
      "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.",
    genres: ['Adventure', 'Drama', 'Sci-Fi']
  },
  {
    id: 9,
    title: 'The Shawshank Redemption (1994)',
    director: 'Frank Darabont',
    summary:
      'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.',
    genres: ['Drama']
  },
  {
    id: 10,
    title: 'Pulp Fiction (1994)',
    director: 'Quentin Tarantino',
    summary:
      'The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption.',
    genres: ['Crime', 'Drama']
  },
  {
    id: 11,
    title: 'Fight Club (1999)',
    director: 'David Fincher',
    summary:
      'An insomniac office worker and a devil-may-care soapmaker form an underground fight club that evolves into something much, much more.',
    genres: ['Drama']
  },
  // BEGIN: GENERATED DATA
  // Generated data 1
  {
    id: 12,
    title: 'The Matrix (1999)',
    director: 'Lana Wachowski, Lilly Wachowski',
    summary:
      'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.',
    genres: ['Action', 'Sci-Fi']
  },
  // Generated data 2
  {
    id: 13,
    title: 'Forrest Gump (1994)',
    director: 'Robert Zemeckis',
    summary:
      'The presidencies of Kennedy and Johnson, the Vietnam War, the Watergate scandal and other historical events unfold through the perspective of an Alabama man with an IQ of 75, whose only desire is to be reunited with his childhood sweetheart.',
    genres: ['Drama', 'Romance']
  },
  // Generated data 3
  {
    id: 14,
    title: 'The Godfather (1972)',
    director: 'Francis Ford Coppola',
    summary:
      'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.',
    genres: ['Crime', 'Drama']
  },
  // Generated data 4
  {
    id: 15,
    title: 'The Shawshank Redemption (1994)',
    director: 'Frank Darabont',
    summary:
      'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.',
    genres: ['Drama']
  },
  // Generated data 5
  {
    id: 16,
    title: 'The Lord of the Rings: The Fellowship of the Ring (2001)',
    director: 'Peter Jackson',
    summary:
      'A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.',
    genres: ['Action', 'Adventure', 'Drama', 'Fantasy']
  },
  // Generated data 6
  {
    id: 17,
    title: 'The Dark Knight Rises (2012)',
    director: 'Christopher Nolan',
    summary:
      "Eight years after the Joker's reign of anarchy, Batman, with the help of the enigmatic Catwoman, is forced from his exile to save Gotham City from the brutal guerrilla terrorist Bane.",
    genres: ['Action', 'Adventure', 'Thriller']
  },
  // Generated data 7
  {
    id: 18,
    title: 'The Godfather: Part II (1974)',
    director: 'Francis Ford Coppola',
    summary:
      'The early life and career of Vito Corleone in 1920s New York City is portrayed, while his son, Michael, expands and tightens his grip on the family crime syndicate.',
    genres: ['Crime', 'Drama']
  },
  // Generated data 8
  {
    id: 19,
    title: 'The Lord of the Rings: The Two Towers (2002)',
    director: 'Peter Jackson',
    summary:
      "While Frodo and Sam edge closer to Mordor with the help of the shifty Gollum, the divided fellowship makes a stand against Sauron's new ally, Saruman, and his hordes of Isengard.",
    genres: ['Action', 'Adventure', 'Drama', 'Fantasy']
  },
  // Generated data 9
  {
    id: 20,
    title: 'The Lord of the Rings: The Return of the King (2003)',
    director: 'Peter Jackson',
    summary:
      "Gandalf and Aragorn lead the World of Men against Sauron's army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.",
    genres: ['Action', 'Adventure', 'Drama', 'Fantasy']
  }
  // END: GENERATED DATA
];

function saveData(data: any) {
  LocalStorage.set('movies', data);
}
function loadData(): MovieData[] {
  if (!LocalStorage.has('movies')) return initialData;
  return LocalStorage.getItem('movies') || [];
}

export const useMoviesStore = defineStore('movies-dummy-dbs', {
  state: () => ({
    data: loadData(),
    lastId: 20
  }),
  getters: {
    getAll: (state) => state.data
  },
  actions: {
    search(text?: string) {
      if (!text) return this.data;
      return this.data.filter((movie: MovieData) => {
        return movie.title.toLowerCase().includes(text.toLowerCase());
      });
    },
    register(movie: Omit<MovieData, 'id'>) {
      const id = this.lastId + 1;
      this.lastId = id;
      this.data.push({ ...movie, id });
      saveData(this.data);
    },
    update(id: number, movie: Partial<MovieData>) {
      const index = this.data.findIndex((movie) => movie.id === id);
      if (index !== -1) {
        this.data[index] = { ...this.data[index], ...movie };
      }
      saveData(this.data);
    },
    remove(id: number) {
      const index = this.data.findIndex((movie) => movie.id === id);
      if (index !== -1) {
        this.data.splice(index, 1);
      }
      saveData(this.data);
    }
  }
});
